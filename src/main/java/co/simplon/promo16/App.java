package co.simplon.promo16;

import co.simplon.promo16.oop.PlayerGame;

import co.simplon.promo16.oop.Grid;

public class App {

    public static void main(String[] args) {
        
        Grid grid = new Grid(6, 6);
        
        // poition des nombres fixes

        grid.pinedNb(1, 6, '1');
        grid.pinedNb(2, 3, '0');
        grid.pinedNb(2, 4, '0');
        grid.pinedNb(4, 1, '1');
        grid.pinedNb(4, 2, '1');
        grid.pinedNb(5, 1, '1');
        grid.pinedNb(5, 5, '0');
        grid.pinedNb(6, 3, '0');

        // position et valeurs des nombres à jouer

        // grid.place(6, 1, '0');
        // grid.place(3, 1, '0');
        // grid.place(1, 1, '0');
        // grid.place(1, 2, '0');
        // grid.place(1, 3, '1');
        // grid.place(1, 4, '1');
        // grid.place(1, 5, '0');
        // grid.place(2, 1, '1');
        // grid.place(2, 2, '1');
        // grid.place(2, 5, '1');
        // grid.place(2, 6, '0');
        // grid.place(3, 1, '0');
        // grid.place(3, 2, '0');
        // grid.place(3, 3, '1');
        // grid.place(3, 4, '0');
        // grid.place(3, 5, '1');
        // grid.place(3, 6, '1');
        // grid.place(4, 3, '0');
        // grid.place(4, 4, '1');
        // grid.place(4, 5, '0');
        // grid.place(4, 6, '0');
        // grid.place(5, 2, '0');
        // grid.place(5, 3, '1');
        // grid.place(5, 4, '1');
        // grid.place(5, 6, '0');
        // grid.place(6, 1, '0');
        // grid.place(6, 2, '1');
        // grid.place(6, 4, '0');
        // grid.place(6, 5, '1');
        
        System.out.println("\n" + "Bienvenue dans ce jeu Takuzu !");
        grid.display();

        PlayerGame complete = new PlayerGame(grid);
        complete.start();
    }
}