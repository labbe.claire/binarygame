package co.simplon.promo16.oop;

import java.util.ArrayList;
import java.util.List;

public class Grid {
    private int rowCount;
    private int colCount;
    private char[][] grid;
    private char[][] gridPined;
    private boolean validation = true;
    private boolean test = true;

    /**
     * initialisation de la grille vierge et de la grille avec nombres épinglés.
     */

    public Grid(int rowCount, int colCount) {
        this.rowCount = rowCount;
        this.colCount = colCount;

        grid = new char [rowCount][colCount];
        gridPined = new char [rowCount][colCount];

        for (int i = 0; i < rowCount; i++) {

            for (int j = 0; j < colCount; j++) {

                grid[i][j] = '.';
                gridPined[i][j] = '.';
            }
        }
    }

    /**
     * Méthode d'affichage de la grille avec séparateurs de colonnes.
     */

    public void display() {
        System.out.println();

        for (int i = 0; i < rowCount; i++) {

            for (int j = 0; j < colCount; j++) {
                System.out.print(" | " + grid[i][j]);
            }
            System.out.println(" | ");
        }
        System.out.println();
    }

    /**
     * Placement des nombres épinglés sur la grille avant que le joueur ne commence la partie.
     */

    public void pinedNb(int r, int c, char t) {
        r = r - 1;
        c = c - 1;

        grid[r][c] = t;
        gridPined[r][c] = t;
    }

    /**
     * Placement du joueur et vérification si pas d'erreur.
     */

    public void place(int r, int c, char t) {
        r = r - 1;
        c = c - 1;

        if (r < 0 || c < 0 || r > rowCount || c > colCount) {
            System.out.println("Erreur. Vous êtes hors jeu !");
            return;
        }
        if (grid[r][c] != '.' && gridPined[r][c] == '0' || gridPined[r][c] == '1') {
            System.out.println(" ");
            System.out.println("\n" + "*** La zone choisie ne peut être modifiée. ***");
            return;
        }
        grid[r][c] = t;
        checkAll();
    }

    /**
     * Vérification si plus de trois nombres identiques sur la même ligne.
     */

    public boolean checkRows() {

        for (int i = 0; i < rowCount; i++) {
            int nb_of_0 = 0;
            int nb_of_1 = 0;
            for (int j = 0; j < colCount; j++) {
                if (grid[i][j] == '0') {
                    nb_of_0++;
                }
                if (grid[i][j] == '1') {
                    nb_of_1++;
                }
            }
            if (nb_of_0 > 3 || nb_of_1 > 3) {
                System.out.println(" ! Erreur sur la ligne " + (i + 1) + " !");
                return test == false;
            }
        }
        return test;
    }

    /**
     * Vérification si plus de trois nombres identiques sur la même colonne.
     */

    public boolean checkColumns() {

        for (int j = 0; j < colCount; j++) {
            int nb_of_0 = 0;
            int nb_of_1 = 0;

            for (int i = 0; i < rowCount; i++) {
                if (grid[i][j] == '0') {
                    nb_of_0++;
                }
                if (grid[i][j] == '1') {
                    nb_of_1++;
                }
            }
            if (nb_of_0 > 3 || nb_of_1 > 3) {
                System.out.println(" ! Erreur sur la colonne " + (j + 1) + " !");
                return test == false;
            }
        }
        return test;
    }

    /**
     * Vérification si trois nombres identiques côte-à-côte sur la même ligne.
     */

    public boolean checkSame3rows() {

        for (int i = 0; i < rowCount; i++) {
            char nbPrec = ' '; // positionné ici pour réinitialiser le nbPrec à chaque ligne
            int counter = 0; // positionné ici pour réinitialiser le counter à chaque ligne
            for (int j = 0; j < colCount; j++) {
                char nbCurrent = grid[i][j];
                if (nbCurrent == nbPrec && nbCurrent != '.') {
                    counter++;
                } else {
                    counter = 1;
                }
                nbPrec = nbCurrent;
                if (counter > 2) {
                    System.out.println("! Trois nombres identiques côte-à-côte sur la ligne " + (i + 1) + " !");
                    return test == false;
                }
            }
        }
        return test;
    }

    /**
     * Vérification si trois nombres identiques côte-à-côte sur la même colonne.
     */

    public boolean checkSame3columns() {
        for (int j = 0; j < colCount; j++) {
            char nbPrec = ' '; // positionné ici pour réinitialiser le nbPrec à chaque colonne
            int counter = 0; // positionné ici pour réinitialiser le counter à chaque colonne
            for (int i = 0; i < colCount; i++) {
                char nbCurrent = grid[i][j];
                if (nbCurrent == nbPrec && nbCurrent != '.') {
                    counter++;
                } else {
                    counter = 1;
                }
                nbPrec = nbCurrent;
                if (counter > 2) {
                    System.out.println("! Trois nombres identiques côte-à-côte sur la colonne " + (j + 1) + " !");
                    return test == false;
                }
            }
        }
        return test;
    }

    /**
     * Vérification si des lignes sont similaires.
     */

    public boolean checkSimilarRows() {
        char PointExcept = '.';
        for (int i = 0; i < rowCount - 1; i++) {
            for (int k = i + 1; k < rowCount; k++) {
                test = true;
                for (int j = 1; j < colCount; j++) {
                    if (grid[i][j] != grid[k][j]) {
                        test = false;
                    }
                    if (grid[i][j] == PointExcept) {
                        test = false;
                    }
                }
                if (test)
                    System.out.println("! Attention ! La ligne " + (i + 1) + " égale à la ligne " + (k + 1));
            }
            if (test)
                return test;
        }
        return test;
    }

    /**
     * Vérification si des colonnes sont similaires.
     */

    public boolean checkSimilarColumns() {
        char PointExcept = '.';
        for (int j = 0; j < colCount - 1; j++) {
            for (int k = j + 1; k < colCount; k++) {
                test = true;
                for (int i = 1; i < rowCount; i++) {
                    if (grid[i][j] != grid[i][k]) {
                        test = false;
                    }
                    if (grid[i][j] == PointExcept) {
                        test = false;
                    }
                }
                if (test)
                    System.out.println("! Attention ! La colonne " + (j + 1) + " égale à la colonne " + (k + 1));
            }
            if (test)
                return test;
        }
        return test;
    }

    /**
     * Méthode regroupant toutes les vérifications précédentes.
     */

    public void checkAll() {
        checkRows();
        checkColumns();
        checkSame3rows();
        checkSame3columns();
        checkSimilarRows();
        checkSimilarColumns();
    }

    /**
     * Méthode pour finir la partie.
     */ 

    public boolean victory() {
        List<Character> temp = new ArrayList<>();
        for (int i = 0; i < rowCount; i++) {
            for (int j = 0; j < colCount; j++) {
                temp.add(grid[i][j]);
            }
        }
        if (temp.contains('.')) {
            validation = false;
        } else if (test) {
            validation = true;
        }
        return validation;

    }
}
